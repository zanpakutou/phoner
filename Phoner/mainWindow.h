#ifndef MainWindow_H
#define MainWindow_H

#include <QWidget>
#include "pjsipmanager.h"
#include "appsettings.h"
#include "dialog.h"
#include "ringspanel.h"
#include "settingwidget.h"
#include <QMenu>
#include "databasecontroller.h"
#include "abstractaudiomanager.h"
#include "contactmanager.h"
#include "dialpad.h"

class VolumeButton;

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void logout();
    void login();
    void openIncomingDialog(Session*);
    void closeIncomingDoalog(Session*);
    void openSettings();
    void Call();
    void hasRegistr(bool has);
    void updateState(Session* pRing);
    void updateMediaState(Session* pRing);
    void addContact();
    void clickContact();
    void clickHistory();
    void contmenu(QPoint);
    void CallContextMenu();
    void DelContact();
    void ClearHistory();
    void CallContextMenuHistory();
    void OpenCloseDilapad();
    void DialpadInsert(QString);

public:
    void replaceModel();

private:
    Ui::MainWindow *ui;
    Dialog* m_incomingDialog;
    SettingWidget* m_settingWidget;
    VolumeButton *m_volumeButtonDynamic;
    VolumeButton *m_volumeButtonMicrophone;
    QMenu* m_menu;
    QMenu* m_contmenu;
    dialpad* m_dialpad;
    typedef QList<RingsPanel*> RingsList;
    RingsList m_ringsPanel;
    void createMenu();
    void createVolumeButton();
    void hideLogout();
    void showLogin();
    void resizeWindow();
    void loadSettings();
    void updateUI(bool t);
    void addRing(Session*);
    void delRing(Session*);
    DataBaseController* m_dbctrl;
    PjsipManager *m_manager;
    AbstractAudioManager* m_audioManager;
    ContactManager* m_contMan;
    QAction* m_ActCall;
    QAction* m_ActDelCon;
    QAction* m_ActClearHistory;
    QAction* m_ActCallHistory;

};

#endif // MainWindow_H
