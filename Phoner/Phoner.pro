#-------------------------------------------------
#
# Project created by QtCreator 2015-10-06T12:21:00
#
#-------------------------------------------------

QT       += core gui sql multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Phoner
TEMPLATE = app

DEFINES += PJ_WIN32=1

SOURCES += main.cpp\
    pjsipmanager.cpp \
    appsettings.cpp \
    dialog.cpp \
    session.cpp \
    volumebutton.cpp \
    ringspanel.cpp \
    settingwidget.cpp \
    utils.cpp \
    mainwindow.cpp \
    pjsipaudiomanager.cpp \
    databasecontroller.cpp \
    contactmanager.cpp \
    dialpad.cpp

HEADERS  += \
    pjsipmanager.h \
    appsettings.h \
    dialog.h \
    session.h \
    volumebutton.h \
    ringspanel.h \
    settingwidget.h \
    utils.h \
    mainwindow.h \
    pjsipaudiomanager.h \
    abstractaudiomanager.h \
    databasecontroller.h \
    contactmanager.h \
    dialpad.h

FORMS    += \
    dialog.ui \
    ringspanel.ui \
    settingwidget.ui \
    mainwindow.ui \
    dialpad.ui

INCLUDEPATH += "c:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\include"
LIBS +=-lmswsock \
-lws2_32 \
-lole32

RESOURCES += \
    res.qrc

INCLUDEPATH += $$PWD/../pjsiplibinclude/include
DEPENDPATH += $$PWD/../pjsiplibinclude/include

win32: LIBS += -L$$PWD/../pjsiplibinclude/lib/ -llibpjproject-i386-Win32-vc8-Debug

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../pjsiplibinclude/lib/libpjproject-i386-Win32-vc8-Debug.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../pjsiplibinclude/lib/liblibpjproject-i386-Win32-vc8-Debug.a

win32: LIBS += -L$$PWD/../pjsiplibinclude/lib/ -llibpjproject-i386-Win32-vc8-Release

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../pjsiplibinclude/lib/libpjproject-i386-Win32-vc8-Release.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../pjsiplibinclude/lib/liblibpjproject-i386-Win32-vc8-Release.a
