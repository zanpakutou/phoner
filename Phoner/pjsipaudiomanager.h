#ifndef PJSIPAUDIOMANAGER
#define PJSIPAUDIOMANAGER
#include "abstractaudiomanager.h"

class PjsipAudioManager: public AbstractAudioManager
{
public:
    int getVolumeDynamic();
    int getVolumeMicrophone();
    QList<AudioDevice> getAudioDeviceList();
    int* getCurrentAudioDevices();
    void changeAudioDevice(int microphone, int dynamic);
    //virtual ~PjsipAudioManager();

public slots:
    void changeVolumeDynamic(int);
    void changeVolumeMicrophone(int);

};

#endif // PJSIPAUDIOMANAGER

