#ifndef SESSION
#define SESSION

#include <pjsua-lib/pjsua.h>
#include <pj/string.h>
#include <QElapsedTimer>
#include <QTime>

#define TYPE_OUTGOING "Outgoing"
#define TYPE_INCOMING "Incoming"

class Session
{

public:
    Session(pjsua_call_id callID);
    ~Session();
    pjsua_call_id getCallID();
    void setCallID(pjsua_call_id CallID);
    QTime getTimer();
    void startTimer();
    void restartTimer();
    char* getRemoteURI();
    QString getCallType();
    pjsip_inv_state getCallState();
    pjsua_call_media_status getCallMediaState();
    void update();
    void setCallState(pjsip_inv_state state);
    void setCallMediaState(pjsua_call_media_status state);

protected:
    pjsua_call_id m_callID;
    QElapsedTimer m_Timer;
    char* m_remoteURI;
    pjsip_inv_state m_callState;
    pjsua_call_media_status m_callMediaState;
    QString m_type;

};

#endif // SESSION

