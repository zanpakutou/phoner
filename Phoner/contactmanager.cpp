#include "contactmanager.h"
#include "utils.h"
#include "appsettings.h"

ContactManager::ContactManager()
{
    m_dbContrl = new DataBaseController();
    QSqlTableModel* contMod = m_dbContrl->getContactModel();
    pjsua_buddy_config buddy_cfg;
    pjsua_buddy_id p_buddy_id;
    for(int i=0; i<contMod->rowCount(); i++)
    {
        QSqlRecord rec = contMod->record(i);
        pj_str_t uri = pj_str(Utils::byteArrayToChar(rec.value("remote_uri").toByteArray()));
        buddy_cfg.uri = uri;
        pj_str_t nickname = pj_str(Utils::byteArrayToChar(rec.value("nickname").toByteArray()));
        //QByteArray nickname = Utils::byteArrayToChar(rec.value("nickname").toByteArray());
        buddy_cfg.user_data = &nickname;
        pjsua_buddy_add(&buddy_cfg,&p_buddy_id);
    }
}

int ContactManager::addContact(QByteArray uri, QByteArray nickname)
{
    uri.push_front("sip:");
    uri.append("@");
    //hardcode
    //uri.append("192.168.0.1");
    uri.append(AppSettings::getAppSettings()->getDomainCh());

    pj_str_t contUri = pj_str(Utils::byteArrayToChar(uri));
    int rc =pjsua_buddy_find(&contUri);
    if(rc > -1)
    {
        return rc;
    }
    pjsua_buddy_config buddy_cfg;
    pjsua_buddy_id p_buddy_id;

    pj_str_t contNickname = pj_str(Utils::byteArrayToChar(nickname));
    QByteArray uri2 = uri;
    QByteArray nickname2 = nickname;

    buddy_cfg.uri = contUri;
    buddy_cfg.user_data = &contNickname;
    pj_status_t stat = pjsua_buddy_add(&buddy_cfg, &p_buddy_id);
    if(stat == PJ_SUCCESS)
    {
        m_dbContrl->addrowContact(uri2, nickname2);
    }
    pjsua_buddy_info info;
    pjsua_buddy_update_pres(p_buddy_id);
    pjsua_buddy_get_info(p_buddy_id, &info);

    return 0;
}

void ContactManager::delContact(QByteArray uri)
{
    /*
    uri.push_front("sip:");
    uri.append("@");
    uri.append(AppSettings::getAppSettings()->getDomainCh());
    */
    pj_str_t contUri = pj_str(Utils::byteArrayToChar(uri));

    pjsua_buddy_id buddyID = pjsua_buddy_find(&contUri);
    pj_status_t stat = pjsua_buddy_del(buddyID);
    if (stat == PJ_SUCCESS)
    {
        //m_dbContrl->delContact(uri);
    }

}
