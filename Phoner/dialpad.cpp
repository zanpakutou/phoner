#include "dialpad.h"
#include "ui_dialpad.h"

dialpad::dialpad(QWidget *parent) :
    QWidget(0, Qt::Tool | Qt::WindowStaysOnTopHint),
    //lastFocusedWidget(0),
    ui(new Ui::dialpad)
{
    ui->setupUi(this);
    connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)),this, SLOT(saveFocusWidget(QWidget*,QWidget*)));
    connect(ui->b1, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b2, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b3, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b4, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b5, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b6, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b7, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b8, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b9, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b10, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b11, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(ui->b12, SIGNAL(clicked()), this, SLOT(buttonClicked()));
}
/*
bool dialpad::event(QEvent *e)
{
    switch (e->type())
    {
    case QEvent::WindowActivate:
        if (lastFocusedWidget)
        {
            lastFocusedWidget->activateWindow();
        }
        break;
    default:
        break;
    }
    return QWidget::event(e);
}

void dialpad::saveFocusWidget(QWidget * , QWidget *newFocus)
{
    if (newFocus != 0 && !this->isAncestorOf(newFocus))
    {
        lastFocusedWidget = newFocus;
    }
}
*/
dialpad::~dialpad()
{
    delete ui;
}

void dialpad::buttonClicked()
{
    QObject* send = sender();
    QString objname = send->objectName();
    objname.remove(0,1);
    int a = objname.toInt();
    switch(a)
    {
    case 1:
        emit characterGenerated("1");
        break;
    case 2:
        emit characterGenerated("2");
        break;
    case 3:
        emit characterGenerated("3");
        break;
    case 4:
        emit characterGenerated("4");
        break;
    case 5:
        emit characterGenerated("5");
        break;
    case 6:
        emit characterGenerated("6");
        break;
    case 7:
        emit characterGenerated("7");
        break;
    case 8:
        emit characterGenerated("8");
        break;
    case 9:
        emit characterGenerated("9");
        break;
    case 10:
        emit characterGenerated("*");
        break;
    case 11:
        emit characterGenerated("0");
        break;
    case 12:
        emit characterGenerated("#");
        break;

    }

    int b = 0;

}
