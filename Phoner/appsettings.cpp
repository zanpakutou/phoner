#include "appsettings.h"

AppSettings* AppSettings::m_instance=0;

AppSettings::AppSettings()
{
    m_psettings = new QSettings("Mera","Phoner");
}

AppSettings* AppSettings::getAppSettings()
{
    if (!m_instance)
    {
        m_instance = new AppSettings;
    }
    return m_instance;
}

QString AppSettings::getLoginStr()
{
    return m_psettings->value(SETTING_LOGIN, "").toString();
}

QString AppSettings::getPasswdStr()
{
    return m_psettings->value(SETTING_PASSWD, "").toString();
}

QString AppSettings::getServerStr()
{
    return m_psettings->value(SETTING_SERVER, "").toString();
}

QString AppSettings::getDomainStr()
{
    return m_psettings->value(SETTING_DOMAIN, "").toString();
}

QString AppSettings::getTransportType()
{
    return m_psettings->value(SETTING_TR_TYPE, "").toString();
}

int AppSettings::getPort()
{
    return m_psettings->value(SETTING_PORT, "").toInt();;
}

char* AppSettings::getLoginCh()
{    
    return Utils::byteArrayToChar(m_psettings->value(SETTING_LOGIN, "").toByteArray());
}

char* AppSettings::getPasswdCh()
{
    return Utils::byteArrayToChar(m_psettings->value(SETTING_PASSWD, "").toByteArray());
}

char* AppSettings::getServerCh()
{
    return Utils::byteArrayToChar(m_psettings->value(SETTING_SERVER, "").toByteArray());
}

char* AppSettings::getDomainCh()
{
    return Utils::byteArrayToChar(m_psettings->value(SETTING_DOMAIN, "").toByteArray());
}

void AppSettings::setDomain(QString str)
{
    m_psettings->setValue(SETTING_DOMAIN, str);
}

void AppSettings::setServer(QString str)
{
    m_psettings->setValue(SETTING_SERVER, str);
}

void AppSettings::setPort(QString str)
{
    m_psettings->setValue(SETTING_PORT, str);
}

void AppSettings::setLogin(QString str)
{
    m_psettings->setValue(SETTING_LOGIN, str);
}

void AppSettings::setPasswd(QString str)
{
    m_psettings->setValue(SETTING_PASSWD, str);
}

void AppSettings::setTransportType(QString str)
{
    m_psettings->setValue(SETTING_TR_TYPE, str);
}

void AppSettings::save()
{
    m_psettings->sync();
}
