#ifndef DIALPAD_H
#define DIALPAD_H

#include <QWidget>
#include <QSignalMapper>

namespace Ui {
class dialpad;
}

class dialpad : public QWidget
{
    Q_OBJECT

public:
    explicit dialpad(QWidget *parent = 0);
    ~dialpad();
signals:
    void characterGenerated(QString str);

protected:
    //bool event(QEvent *e);

private slots:
    //void saveFocusWidget(QWidget *oldFocus, QWidget *newFocus);
    void buttonClicked();

private:
    QWidget *lastFocusedWidget;
    QSignalMapper signalMapper;
    Ui::dialpad *ui;
};

#endif // DIALPAD_H
