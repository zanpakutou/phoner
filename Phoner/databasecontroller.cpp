#include "databasecontroller.h"
#include <QDateTime>
#include "appsettings.h"

DataBaseController::DataBaseController(QObject *parent)
{
    m_appDataBase = QSqlDatabase::addDatabase("QSQLITE");
    m_appDataBase.setDatabaseName("DataBaseApp.db");
    if (m_appDataBase.open())
    {
        if(m_appDataBase.record("history").isEmpty())
        {
            QSqlQuery* query = new QSqlQuery("CREATE TABLE history (remote_uri VARCHAR(256), type VARCHAR(32), time VARCHAR(16), date VARCHAR(256))", m_appDataBase);
        }
        m_historyModel = new QSqlTableModel(this, m_appDataBase);
        m_historyModel->setTable("history");
        m_historyModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
        m_historyModel->select();
        if(m_appDataBase.record("contacts").isEmpty())
        {
            QSqlQuery* query = new QSqlQuery("CREATE TABLE contacts (nickname VARCHAR(256), remote_uri VARCHAR(256))", m_appDataBase);
        }
        m_contactModel = new QSqlTableModel(this, m_appDataBase);
        m_contactModel->setTable("contacts");
        m_contactModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
        m_contactModel->select();
    }
}

void DataBaseController::addrowHistory(Session *ptr)
{
    QSqlQuery* query = new QSqlQuery(m_appDataBase);
    bool t = query->prepare("INSERT INTO history (remote_uri, type, time, date) "
                      "VALUES (:remote_uri, :type, :time, :date)");
    query->bindValue(":remote_uri", ptr->getRemoteURI());
    query->bindValue(":type", ptr->getCallType());
    query->bindValue(":time", ptr->getTimer().toString());
    query->bindValue(":date", QDateTime::currentDateTime().toString("dd.MM.yy hh:mm"));
    query->exec();
}

void DataBaseController::clearHistory()
{
    m_historyModel->removeRows(0,m_historyModel->rowCount());
    m_historyModel->submitAll();
}

QSqlTableModel* DataBaseController::getHistoryModel()
{
    return m_historyModel;
}

QSqlTableModel* DataBaseController::getContactModel()
{
    return m_contactModel;
}

void DataBaseController::addrowContact(QByteArray uri, QByteArray nickname)
{    
    uri.push_front("sip:");
    uri.append("@");
    uri.append(AppSettings::getAppSettings()->getDomainCh());
    QSqlQuery* query = new QSqlQuery(m_appDataBase);
    bool t = query->prepare("INSERT INTO contacts (nickname, remote_uri) "
                      "VALUES (:nickname, :remote_uri)");
    query->bindValue(":nickname", nickname);
    query->bindValue(":remote_uri", uri);
    query->exec();
}

void DataBaseController::delContact(int row)
{
    /*
    QSqlQuery* query = new QSqlQuery(m_appDataBase);
    QString qu = "DELETE FROM contacts  WHERE remote_uri = '";
    qu.push_back(uri);
    qu.push_back("'");
    bool t = query->prepare(qu);
    bool res = query->exec();
    */
    /*
    QString f = "remote_uri='";
    f.push_back(uri);
    f.push_back("'");
    m_contactModel->setFilter(f);
    m_contactModel->select();
    m_contactModel->removeRows(0,m_contactModel->rowCount());
    m_contactModel->submitAll();
    */
    m_contactModel->removeRow(row);
    m_contactModel->submitAll();

}


