#include "mainwindow.h"
#include "ui_MainWindow.h"
#include "volumebutton.h"
#include <QtWidgets>
#include "pjsipaudiomanager.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow),
    m_incomingDialog(NULL)
{
    ui->setupUi(this);
    m_manager = PjsipManager::getInstance();
    m_manager->init();
    m_dbctrl = new DataBaseController(this);
    m_audioManager = new PjsipAudioManager();
    m_dialpad = new dialpad(this);
    createVolumeButton();
    createMenu();
    loadSettings();
    m_settingWidget = new SettingWidget();
    ui->lineVertical->hide();
    ui->pushButtonDialPad->setIcon(QIcon(":/res/icons/dialpad.png"));
    ui->pushButtonRing->setIcon(QIcon(":/res/icons/answer.png"));
    ui->lineEditPasswd->setEchoMode(QLineEdit::Password);
    //hideLogout();
    showLogin();
    //m_manager->registration();
    //m_contMan = new ContactManager();
    ui->treeView->setEnabled(false);
    connect(m_manager, SIGNAL(hasRegistration(bool)), this, SLOT(hasRegistr(bool)));
    connect(m_manager, SIGNAL(updateState(Session*)), this, SLOT(updateState(Session*)));
    connect(m_manager, SIGNAL(updateMediaState(Session*)), this, SLOT(updateMediaState(Session*)));
    connect(ui->pushButtonLogin, SIGNAL(clicked()), this, SLOT(login()));
    connect(ui->pushButtonRing, SIGNAL(clicked()), this, SLOT(Call()));
    connect(ui->lineEditInsert, SIGNAL(returnPressed()), this, SLOT(Call()));
    connect(ui->pushButtonContacts, SIGNAL(clicked()), this, SLOT(clickContact()));
    connect(ui->pushButtonHistory, SIGNAL(clicked()), this, SLOT(clickHistory()));
    ui->pushButtonContacts->setChecked(true);
    replaceModel();
    connect(ui->pushButtonSaveCont, SIGNAL(clicked()), this, SLOT(addContact()));
    connect(ui->treeView, SIGNAL(customContextMenuRequested(QPoint)),this, SLOT(contmenu(QPoint)));
    connect(ui->pushButtonDialPad, SIGNAL(clicked()),this, SLOT(OpenCloseDilapad()));
    connect(m_dialpad, SIGNAL(characterGenerated(QString)), this, SLOT(DialpadInsert(QString)));
}

MainWindow::~MainWindow()
{    
    m_manager->destroy();
    RingsList::iterator it = m_ringsPanel.begin();
    for (it; it!=m_ringsPanel.end(); it++)
    {
        (*it)->deleteLater();
    }
    delete ui;
}

void MainWindow::createVolumeButton()
{
    m_volumeButtonDynamic = new VolumeButton(this, ":/res/icons/volume.png");
    m_volumeButtonDynamic->setGeometry(((this->geometry().width())-30),0,25,25);

    m_volumeButtonMicrophone = new VolumeButton(this, ":/res/icons/microphone.png");
    m_volumeButtonMicrophone->setGeometry(((this->geometry().width())-60),0,25,25);

    connect(m_volumeButtonDynamic, SIGNAL(volumeChanged(int)), m_audioManager, SLOT(changeVolumeDynamic(int)));
    connect(m_volumeButtonMicrophone, SIGNAL(volumeChanged(int)), m_audioManager, SLOT(changeVolumeMicrophone(int)));
    //ui->volumeButton->setToolTip(tr("Adjust volume"));
    //volumeButton->setVolume(mediaPlayer.volume());

    m_volumeButtonDynamic->setVolume(m_audioManager->getVolumeDynamic());
    m_volumeButtonMicrophone->setVolume(m_audioManager->getVolumeMicrophone());


    QShortcut *increaseShortcutDynamic = new QShortcut(Qt::Key_Up, this);
    connect(increaseShortcutDynamic, &QShortcut::activated, m_volumeButtonDynamic, &VolumeButton::increaseVolume);

    QShortcut *decreaseShortcutDynamic = new QShortcut(Qt::Key_Down, this);
    connect(decreaseShortcutDynamic, &QShortcut::activated, m_volumeButtonDynamic, &VolumeButton::descreaseVolume);

    QShortcut *increaseShortcutMicrophone = new QShortcut(Qt::Key_Up, this);
    connect(increaseShortcutMicrophone, &QShortcut::activated, m_volumeButtonMicrophone, &VolumeButton::increaseVolume);

    QShortcut *decreaseShortcutMicrophone = new QShortcut(Qt::Key_Down, this);
    connect(decreaseShortcutMicrophone, &QShortcut::activated, m_volumeButtonMicrophone, &VolumeButton::descreaseVolume);
}

void MainWindow::createMenu()
{
    ui->toolButton->setIcon(QIcon(":/res/icons/menu.png"));
    m_menu = new QMenu(ui->toolButton);
    m_menu->addAction("&Settings",this, SLOT(openSettings()));
    m_menu->addAction("&Logout",this,SLOT(logout()));
    m_menu->addAction("&Exit",this, SLOT(close()));
    ui->toolButton->setMenu(m_menu);
    m_contmenu = new QMenu(ui->treeView);
    m_ActCall = new QAction(QIcon(":/res/icons/answer.png"), tr("&Call"), ui->treeView);
    m_ActCallHistory = new QAction(QIcon(":/res/icons/answer.png"), tr("&Call"), ui->treeView);
    m_ActClearHistory = new QAction(tr("&Clear history"), ui->treeView);
    m_ActDelCon = new QAction(tr("&Delete contact"), ui->treeView);
    connect(m_ActCall, SIGNAL(triggered()), this, SLOT(CallContextMenu()));
    connect(m_ActCallHistory, SIGNAL(triggered()), this, SLOT(CallContextMenuHistory()));
    connect(m_ActClearHistory, SIGNAL(triggered()), this, SLOT(ClearHistory()));
    connect(m_ActDelCon, SIGNAL(triggered()), this, SLOT(DelContact()));

}

void MainWindow::logout()
{
    m_manager->logout();
}

void MainWindow::hideLogout()
{
    ui->labelLogin->hide();
    ui->labelPasswd->hide();
    ui->labelUsername->show();
    ui->lineEditLogin->hide();
    ui->lineEditPasswd->hide();
    ui->pushButtonLogin->hide();
    m_volumeButtonDynamic->show();
    m_volumeButtonMicrophone->show();
}

void MainWindow::showLogin()
{
    ui->labelLogin->show();
    ui->labelPasswd->show();
    ui->labelUsername->hide();
    ui->lineEditLogin->show();
    ui->lineEditPasswd->show();
    ui->pushButtonLogin->show();
    m_volumeButtonDynamic->hide();
    m_volumeButtonMicrophone->hide();
}

void MainWindow::openIncomingDialog(Session* pRing)
{
    m_incomingDialog = new Dialog(this, pRing->getRemoteURI(), pRing->getCallID());
    m_incomingDialog->show();
}

void MainWindow::closeIncomingDoalog(Session* pRing)
{
    m_incomingDialog->close();
}

void MainWindow::login()
{
    AppSettings* settings = AppSettings::getAppSettings();

    settings->setLogin(ui->lineEditLogin->text());
    settings->setPasswd(ui->lineEditPasswd->text());
    settings->save();
    ui->labelUsername->setText(settings->getLoginStr());
    m_manager->registration();
}

void MainWindow::openSettings()
{
    m_settingWidget->show();
}

void MainWindow::resizeWindow()
{
    this->resize(700, this->geometry().height());
    ui->lineVertical->show();
    m_volumeButtonDynamic->setGeometry(((this->geometry().width())-30),0,25,25);
    m_volumeButtonMicrophone->setGeometry(((this->geometry().width())-60),0,25,25);
}

void MainWindow::loadSettings()
{
    AppSettings* setting = AppSettings::getAppSettings();
    ui->labelUsername->setText(setting->getLoginStr());
    ui->lineEditLogin->setText(setting->getLoginStr());
    ui->lineEditPasswd->setText(setting->getPasswdStr());
}

void MainWindow::addRing(Session* pRing)
{
    m_ringsPanel.push_back(new RingsPanel(this, pRing));
    ui->verticalLayoutRing->addWidget(m_ringsPanel.back());
    m_ringsPanel.back()->show();
    resizeWindow();
}

void MainWindow::delRing(Session* pRing)
{
    m_dbctrl->addrowHistory(pRing);
    RingsList::iterator it = m_ringsPanel.begin();
    for (it; it!=m_ringsPanel.end(); it++)
    {
        if ((*it)->m_pSess->getRemoteURI() == pRing->getRemoteURI())
        {
            (*it)->hide();

            (*it)->deleteLater();
            it = m_ringsPanel.erase(it);
            break;
        }
    }
}

void MainWindow::Call()
{
    m_manager->makeCall(ui->lineEditInsert->text());
}

void MainWindow::hasRegistr(bool has)
{
    if (has)
    {
       hideLogout();
       updateUI(false);
       ui->treeView->setEnabled(true);
    }
    else
    {
        showLogin();
        updateUI(true);
        ui->treeView->setEnabled(false);
        if(!m_ringsPanel.isEmpty())
        {
            m_manager->hangUpALLcall();

            RingsList::iterator it = m_ringsPanel.begin();
            while(!m_ringsPanel.isEmpty())
            {
               (*it)->hide();
                m_dbctrl->addrowHistory((*it)->m_pSess);
               (*it)->deleteLater();
               it = m_ringsPanel.erase(it);
            }

        }
    }
}

void MainWindow::updateUI(bool t)
{
    ui->pushButtonDialPad->setDisabled(t);
    ui->pushButtonRing->setDisabled(t);
    ui->lineEditInsert->setDisabled(t);
}

void MainWindow::updateState(Session *pRing)
{
    if (pRing->getCallState() == PJSIP_INV_STATE_INCOMING)
    {
        addRing(pRing);
        openIncomingDialog(pRing);
    }
    else if (pRing->getCallState() == PJSIP_INV_STATE_CALLING)
    {
        addRing(pRing);
    }
    else if (pRing->getCallState() == PJSIP_INV_STATE_DISCONNECTED)
    {
        delRing(pRing);
        if (m_incomingDialog)
        {
            m_incomingDialog->close();
        }
        delete pRing;
        pRing = NULL;
    }

    if(pRing)
    {
        RingsList::iterator it = m_ringsPanel.begin();

        for (it; it != m_ringsPanel.end(); it++)
        {
            if ((*it)->m_pSess->getCallID() == pRing->getCallID())
            {
                (*it)->updateStateRPanel(pRing->getCallState());
                break;
            }
        }
    }
}

void MainWindow::updateMediaState(Session* pRing)
{
    RingsList::iterator it = m_ringsPanel.begin();

    for (it; it != m_ringsPanel.end(); it++)
    {
        if ((*it)->m_pSess->getCallID() == pRing->getCallID())
        {
            (*it)->updateMediaStateRPanel(pRing->getCallMediaState());
            break;
        }
    }
}

void MainWindow::addContact()
{
    bool bOk;
    QString nickname = QInputDialog::getText( 0,
                                         "Input",
                                         "Nickname:",
                                         QLineEdit::Normal,
                                         "enter nickname",
                                         &bOk
                                        );
    if (bOk)
    {
        //m_contMan->addContact(ui->lineEditInsert->text().toUtf8(), nickname.toUtf8());
        m_dbctrl->addrowContact(ui->lineEditInsert->text().toUtf8(), nickname.toUtf8());
    }

}

void MainWindow::replaceModel()
{
    if (ui->pushButtonHistory->isChecked())
    {
        ui->treeView->reset();
        ui->treeView->setModel(m_dbctrl->getHistoryModel());
        ui->treeView->setColumnWidth(0, 130);
        ui->treeView->setColumnWidth(1, 60);
        ui->treeView->setColumnWidth(2, 50);
        ui->treeView->setColumnWidth(3, 80);
        m_contmenu->addAction(m_ActCallHistory);
        m_contmenu->addAction(m_ActClearHistory);
        m_contmenu->removeAction(m_ActCall);
        m_contmenu->removeAction(m_ActDelCon);
    }
    else if (ui->pushButtonContacts->isChecked())
    {
        ui->treeView->reset();
        ui->treeView->setModel(m_dbctrl->getContactModel());
        m_contmenu->addAction(m_ActCall);
        m_contmenu->addAction(m_ActDelCon);
        m_contmenu->removeAction(m_ActCallHistory);
        m_contmenu->removeAction(m_ActClearHistory);

    }
}

void MainWindow::clickHistory()
{
    ui->pushButtonContacts->setChecked(false);
    replaceModel();
}

void MainWindow::clickContact()
{
    ui->pushButtonHistory->setChecked(false);
    replaceModel();
}

void MainWindow::contmenu(QPoint p)
{
    QPoint g = QWidget::mapToParent(p);
    g.setX(g.x()+ 5);
    g.setY(g.y() + 100);
    if(m_contmenu)
    {
        m_contmenu->popup(g);
    }
}

void MainWindow::DelContact()
{
    QModelIndex ind = ui->treeView->currentIndex();
    QModelIndex ind2 = m_dbctrl->getContactModel()->index(ind.row(), 1);
    QByteArray uri = m_dbctrl->getContactModel()->data(ind2).toByteArray();
    //m_contMan->delContact(uri);
    //m_dbctrl->delContact(uri);
    m_dbctrl->delContact(ind.row());

}

void MainWindow::CallContextMenu()
{
    QModelIndex ind = ui->treeView->currentIndex();
    QModelIndex ind2 = m_dbctrl->getContactModel()->index(ind.row(), 1);
    QString uri = m_dbctrl->getContactModel()->data(ind2).toString();
    m_manager->makeCall(uri);
}
void MainWindow::CallContextMenuHistory()
{
    QModelIndex ind = ui->treeView->currentIndex();
    int i = ind.row();
    QModelIndex ind2 = m_dbctrl->getHistoryModel()->index(ind.row(), 0);
    QString uri = m_dbctrl->getHistoryModel()->data(ind2).toString();
    m_manager->makeCall(uri);
}

void MainWindow::ClearHistory()
{
    m_dbctrl->clearHistory();
}

void MainWindow::OpenCloseDilapad()
{
    QPoint coord = this->pos();
    m_dialpad->setGeometry(coord.x() + 10, coord.y() + 120, 135, 165);
    if (m_dialpad->isVisible())
    {
        m_dialpad->hide();
    }
    else
    {
        m_dialpad->show();
    }
}

void MainWindow::DialpadInsert(QString str)
{
    QString oldtext = ui->lineEditInsert->text();
    oldtext.push_back(str);
    ui->lineEditInsert->setText(oldtext);
}
