#ifndef PJSIPMANAGER
#define PJSIPMANAGER

#include <pjsua-lib/pjsua.h>
#include <pj/string.h>
#include <QList>
#include "appsettings.h"
#include "session.h"


class PjsipManager: public QObject
{
    Q_OBJECT

public:
    int init();
    void registration();
    void logout();
    void createTransport();
    void closeTransport();
    void makeCall(QString dst_uri);
    void hangUpCall(pjsua_call_id callID);
    void hangUpALLcall();
    void callAnswer(pjsua_call_id callID);
    void hold(pjsua_call_id callID);
    void unhold(pjsua_call_id callID);


public:
    static PjsipManager* getInstance();
    static void destroy();
    static void onCallState(pjsua_call_id callID, pjsip_event *e);
    static void onIncomingCall(pjsua_acc_id accID, pjsua_call_id callID, pjsip_rx_data *rdata);
    static void onCallMediaState(pjsua_call_id callID);
    static void onRegState2(pjsua_acc_id accID, pjsua_reg_info *info);
    static void onTranspState(pjsip_transport *tp, pjsip_transport_state state, const pjsip_transport_state_info *info);

signals:
    void hasRegistration(bool reg);
    void updateState(Session* pRing);
    void updateMediaState(Session* pRing);
    void error(QString);

protected:
    PjsipManager(QObject *parent = 0);
    static PjsipManager* m_instance;
    virtual ~PjsipManager(){}

private:
    typedef QList<Session*> SessionList;
    SessionList m_session;
    pjsua_acc_id m_pjAccID;
    pjsua_transport_id m_pjTrID;
    bool m_hasTransport;
    void logerror(pj_status_t st);
    void uriAddTrType(QByteArray &arr);
    void uriAddTrType(QString &str);
};

#endif // PJSIPMANAGER

