#ifndef SETTINGWIDGET_H
#define SETTINGWIDGET_H

#include <QDialog>
#include "appsettings.h"
#include "abstractaudiomanager.h"


namespace Ui {
class SettingWidget;
}

class SettingWidget : public QDialog
{
    Q_OBJECT

public:
    explicit SettingWidget(QWidget *parent = 0);
    ~SettingWidget();

public slots:
    void saveSettings();
    void hasRegistr(bool has);

private:
    Ui::SettingWidget *ui;
    void loadSetting();
    AbstractAudioManager* m_audioManager;
    QList<AbstractAudioManager::AudioDevice> m_deviceList;

};

#endif // SETTINGWIDGET_H
