#ifndef VOLUMEBUTTON
#define VOLUMEBUTTON

#include <QToolButton>

QT_FORWARD_DECLARE_CLASS(QMenu)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QSlider)

class VolumeButton : public QToolButton
{
    Q_OBJECT
    Q_PROPERTY(int volume READ volume WRITE setVolume NOTIFY volumeChanged)

public:
    VolumeButton(QWidget *parent, QString filename);
    int volume() const;

public slots:
    void increaseVolume();
    void descreaseVolume();
    void setVolume(int volume);
//    void stylize();

signals:
    void volumeChanged(int volume);

private:
    QMenu *menu;
    QLabel *label;
    QSlider *slider;
    QString filename;
};

#endif // VOLUMEBUTTON

