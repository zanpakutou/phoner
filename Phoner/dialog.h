#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "pjsipmanager.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    Dialog(QWidget *parent, char* labIncall, pjsua_call_id callID);
    ~Dialog();

public slots:
    void clickButAnswer();
    void clickButHangup();

private:
    Ui::Dialog *ui;
    pjsua_call_id m_callID;
};

#endif // DIALOG_H
