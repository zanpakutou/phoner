#ifndef CONTACTMANAGER
#define CONTACTMANAGER

#include <pjsua-lib/pjsua.h>
#include <pj/string.h>
#include "databasecontroller.h"
#include <QByteArray>

class ContactManager
{
public:
    ContactManager();
    int addContact(QByteArray uri, QByteArray nickname);
    void delContact(QByteArray uri);
private:
    DataBaseController* m_dbContrl;
};

#endif // CONTACTMANAGER

