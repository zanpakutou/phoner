#include "pjsipaudiomanager.h"
#include <pjsua-lib/pjsua.h>
#include <pj/string.h>

QList<AbstractAudioManager::AudioDevice> PjsipAudioManager::getAudioDeviceList()
{
    pjmedia_aud_dev_info info[20];
    unsigned int b = 20;
    unsigned int *a = &b;
    pjsua_enum_aud_devs(info, a);
    QList<AbstractAudioManager::AudioDevice> ListAudioDevice;
    AbstractAudioManager::AudioDevice structAudioDevice;
    for (int i=0; i<(*a); i++)
    {
        structAudioDevice.id = i;
        structAudioDevice.name = info[i].name;        
        if(info[i].input_count!=0 && info[i].output_count!=0)
        {
            structAudioDevice.type = InOut;
        }
        else if(info[i].input_count!=0)
        {
            structAudioDevice.type = Input;
        }
        else if(info[i].output_count!=0)
        {
            structAudioDevice.type = Output;
        }
        if(structAudioDevice.name != "Wave mapper")
        {
            ListAudioDevice.append(structAudioDevice);
        }
    }
    return ListAudioDevice;
}

int PjsipAudioManager::getVolumeMicrophone()
{
    int pVal = 50;
    pjsua_snd_get_setting(PJMEDIA_AUD_DEV_CAP_INPUT_VOLUME_SETTING, &pVal);
    return pVal;
}

int PjsipAudioManager::getVolumeDynamic()
{
    int pVal = 50;
    pjsua_snd_get_setting(PJMEDIA_AUD_DEV_CAP_OUTPUT_VOLUME_SETTING, &pVal);
    return pVal;
}

void PjsipAudioManager::changeVolumeMicrophone(int Vol)
{
    pjmedia_aud_param param;
    param.input_vol = Vol;
    pj_status_t stat = pjmedia_aud_param_set_cap(&param,PJMEDIA_AUD_DEV_CAP_INPUT_VOLUME_SETTING, &Vol);

    bool micr = pjsua_snd_is_active();
    pj_status_t st = pjsua_snd_set_setting(PJMEDIA_AUD_DEV_CAP_INPUT_VOLUME_SETTING, &Vol, true);
}

void PjsipAudioManager::changeVolumeDynamic(int Vol)
{
    pjsua_snd_set_setting(PJMEDIA_AUD_DEV_CAP_OUTPUT_VOLUME_SETTING, &Vol, true);
}

void PjsipAudioManager::changeAudioDevice(int microphone, int dynamic)
{
    pjsua_set_snd_dev(microphone,dynamic);
}

int* PjsipAudioManager::getCurrentAudioDevices()
{
    int a = -2;
    int b = -2;
    int* capture_dev = &a;
    int* playback_dev = &b;
    pj_status_t status = pjsua_get_snd_dev(capture_dev, playback_dev);
    int indexs[] = {b, a};
    return indexs;
}
