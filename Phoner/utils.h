#ifndef UTILS
#define UTILS
#include <QByteArray>

class Utils
{
public:
    static char* byteArrayToChar(QByteArray ba);
};

#endif // UTILS

