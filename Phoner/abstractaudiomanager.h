#ifndef ABSTRACTAUDIOMANAGER
#define ABSTRACTAUDIOMANAGER
#include <QObject>

class AbstractAudioManager: public QObject
{
    Q_OBJECT

public:
    virtual int getVolumeDynamic()=0;
    virtual int getVolumeMicrophone()=0;
    enum DeviceType {Output, Input, InOut};
    struct AudioDevice
    {
        int id;
        QString name;
        DeviceType type;
    };
    virtual QList<AudioDevice> getAudioDeviceList()=0;
    virtual void changeAudioDevice(int microphone, int dynamic)=0;
    virtual int* getCurrentAudioDevices()=0;
    virtual ~AbstractAudioManager(){};

public slots:
    virtual void changeVolumeDynamic(int)=0;
    virtual void changeVolumeMicrophone(int)=0;
};

#endif // ABSTRACTAUDIOMANAGER

