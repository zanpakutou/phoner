#ifndef RINGSPANEL_H
#define RINGSPANEL_H

#include <QWidget>
#include <QTimer>
#include "pjsipmanager.h"

namespace Ui {
class RingsPanel;
}

class Session;

class RingsPanel : public QWidget
{
    Q_OBJECT

public:    
    RingsPanel(QWidget *parent, Session* pSess);
    ~RingsPanel();
    Session* m_pSess;
    void updateStateRPanel(pjsip_inv_state state);
    void updateMediaStateRPanel(pjsua_call_media_status state);
    void startUItimer();

public slots:
    void updateTimer();
    void hangUp();
    void hold();

private:
    Ui::RingsPanel *ui;
    QTimer* m_timer;

};

#endif // RINGSPANEL_H
