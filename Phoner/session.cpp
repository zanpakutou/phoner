#include "session.h"

Session::Session(pjsua_call_id callID)
{
    m_callID = callID;

    pjsua_call_info ci;
    pjsua_call_get_info(m_callID, &ci);

    m_callState = ci.state;
    m_remoteURI = new char[ci.remote_info.slen + 1];
    strcpy(m_remoteURI, ci.remote_info.ptr);
    m_callMediaState = ci.media_status;
    if (ci.state == PJSIP_INV_STATE_CALLING)
    {
        m_type = TYPE_OUTGOING;
    }
    else if (ci.state == PJSIP_INV_STATE_INCOMING)
    {
        m_type = TYPE_INCOMING;
    }
}

Session::~Session()
{
    delete []m_remoteURI;
    m_remoteURI = NULL;
}

pjsua_call_id Session::getCallID()
{
    return m_callID;
}

void Session::setCallID(pjsua_call_id CallID)
{
    m_callID = CallID;
}

QTime Session::getTimer()
{
    int timer = m_Timer.elapsed();
    QTime ti(0,0,0);
    QTime time = ti.addMSecs(timer);
    return time;
}

void Session::startTimer()
{
    m_Timer.start();        //mb need to delete.. see str87
}

void Session::restartTimer()
{
    m_Timer.restart();      //mb need to delete.. see str91
}

char* Session::getRemoteURI()
{
    return m_remoteURI;
}

pjsip_inv_state Session::getCallState()
{
    return m_callState;
}

pjsua_call_media_status Session::getCallMediaState()
{
    return m_callMediaState;
}

void Session::update()
{
    pjsua_call_info ci;
    pjsua_call_get_info(m_callID, &ci);

    m_callState = ci.state;
    m_callMediaState = ci.media_status;
}


void Session::setCallMediaState(pjsua_call_media_status state)
{
    m_callMediaState = state;
}

void Session::setCallState(pjsip_inv_state state)
{
    m_callState = state;
    if(m_type == TYPE_INCOMING && state == PJSIP_INV_STATE_CONFIRMED)
    {
        startTimer();
    }
    else if(m_type == TYPE_INCOMING && state == PJSIP_INV_STATE_DISCONNECTED && m_callMediaState == PJSUA_CALL_MEDIA_NONE)
    {
        startTimer();
    }
    else if(m_type == TYPE_OUTGOING && state == PJSIP_INV_STATE_CALLING)
    {
        startTimer();
    }
    else if(m_type == TYPE_OUTGOING && state == PJSIP_INV_STATE_CONFIRMED)
    {
        restartTimer();
    }
}

QString Session::getCallType()
{
    return m_type;
}
