#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationName("Mera");
    QApplication::setApplicationName("Phoner");

    MainWindow w;
    w.show();

    return a.exec();
}
