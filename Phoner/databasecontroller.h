#ifndef DATABASECONTROLLER
#define DATABASECONTROLLER

#include <QtSql>
#include "session.h"

class DataBaseController: public QObject
{
public:
    DataBaseController(QObject *parent = 0);
    void addrowHistory(Session* ptr);
    void clearHistory();
    QSqlTableModel* getHistoryModel();
    void refreshHistory();
    void addrowContact(QByteArray uri, QByteArray nickname);
    void delContact(int row);
    QSqlTableModel* getContactModel();


private:
    QSqlDatabase m_appDataBase;
    QSqlTableModel* m_historyModel;
    QSqlTableModel* m_contactModel;
};

#endif // DATABASECONTROLLER

