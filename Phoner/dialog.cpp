#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::Dialog(QWidget *parent, char* labIncall, pjsua_call_id callID) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    m_callID = callID;
    QString str = "Incoming call: ";
    str += labIncall;
    ui->labIncoming->setText(str);
    ui->butAnswer->setIcon(QIcon(":/res/icons/answer.png"));
    ui->butHangUp->setIcon(QIcon(":/res/icons/hangup.png"));
    connect(ui->butAnswer, SIGNAL(clicked()), this, SLOT(clickButAnswer()));
    connect(ui->butHangUp, SIGNAL(clicked()), this, SLOT(clickButHangup()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::clickButAnswer()
{
    PjsipManager::getInstance()->callAnswer(m_callID);
    close();
}

void Dialog::clickButHangup()
{
    PjsipManager::getInstance()->hangUpCall(m_callID);
    close();
}
