#include "pjsipmanager.h"
#include <QDebug>
#include <QtMultimedia/QAudioDeviceInfo>
const QString TR_TYPE_TCP = "TCP";
const QString TR_TYPE_UDP = "UDP";
const QString TR_TYPE_TLS = "TLS";
const int INVALID = -1;

PjsipManager* PjsipManager::m_instance=0;

PjsipManager* PjsipManager::getInstance()
{
    if (!m_instance)
    {
        m_instance = new PjsipManager;
    }
    return m_instance;
}

PjsipManager::PjsipManager(QObject *parent)
{
    qRegisterMetaType<pjsua_call_id>("pjsua_call_id");
    qRegisterMetaType<pjsip_inv_state>("pjsip_inv_state");
    qRegisterMetaType<pjsua_call_media_status>("pjsua_call_media_status");
    m_pjAccID = INVALID;
    m_pjTrID = INVALID;
    m_hasTransport = false;
}

void PjsipManager::destroy()
{
    pjsua_destroy();
}

int PjsipManager::init()
{
    pjsua_config cfg;
    pjsua_logging_config log_cfg;

    pj_status_t status = pjsua_create();

    if(status!=PJ_SUCCESS)
    {
        return status;
    }

    pjsua_config_default(&cfg);
    cfg.cb.on_incoming_call = &onIncomingCall;
    cfg.cb.on_call_media_state = &onCallMediaState;
    cfg.cb.on_call_state = &onCallState;
    cfg.cb.on_reg_state2 = &onRegState2;
    cfg.cb.on_transport_state = &onTranspState;

    pjsua_logging_config_default(&log_cfg);
    log_cfg.console_level = 4;

    status = pjsua_init(&cfg, &log_cfg, NULL);

    if(status!=PJ_SUCCESS)
    {
        return status;
    }

    status = pjsua_start();
    if(status!=PJ_SUCCESS)
    {
        return status;
    }

    return 0;
}


void PjsipManager::registration()
{
    if (!m_hasTransport)
    {
        createTransport();
    }

    AppSettings* setting = AppSettings::getAppSettings();

    char* sip_server = setting->getServerCh();
    char* sip_login = setting->getLoginCh();
    char* sip_passwd = setting->getPasswdCh();

    QByteArray id;
    id.append("sip:");
    id.append(sip_login);
    id.append("@");
    id.append(sip_server);
    char* sip_id = Utils::byteArrayToChar(id);

    QByteArray uri;
    uri.append("sip:"); uri.append(sip_server); uriAddTrType(uri);
    char* sip_uri = Utils::byteArrayToChar(uri);

    pjsua_acc_config acc_cfg;
    pjsua_acc_config_default(&acc_cfg);

    acc_cfg.id = pj_str(sip_id);
    acc_cfg.reg_uri = pj_str(sip_uri);
    acc_cfg.cred_count = 1;
    acc_cfg.cred_info[0].realm = pj_str(const_cast<char*>("*"));
    acc_cfg.cred_info[0].scheme = pj_str(const_cast<char*>("digest"));
    acc_cfg.cred_info[0].username = pj_str(sip_login);
    acc_cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    acc_cfg.cred_info[0].data = pj_str(sip_passwd);

    pj_status_t status = pjsua_acc_add(&acc_cfg, PJ_TRUE, &m_pjAccID);
    logerror(status);

    delete []sip_server;
    delete []sip_login;
    delete []sip_passwd;
    delete []sip_id;
    delete []sip_uri;
}

void PjsipManager::createTransport()
{
    AppSettings* setting = AppSettings::getAppSettings();

    int sip_port = setting->getPort();
    QString tr_type = setting->getTransportType();

    pj_status_t status;

    pjsua_transport_config tr_cfg;
    pjsua_transport_config_default(&tr_cfg);
    tr_cfg.port = sip_port;

    if (tr_type == TR_TYPE_UDP)
    {
        status = pjsua_transport_create(PJSIP_TRANSPORT_UDP, &tr_cfg, &m_pjTrID);
    }
    else if (tr_type == TR_TYPE_TCP)
    {
        status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &tr_cfg, &m_pjTrID);
    }
    else if (tr_type == TR_TYPE_TLS)
    {
        qInfo() << "TLS not implemented";
        //status = pjsua_transport_create(PJSIP_TRANSPORT_TLS, &tr_cfg, &m_pj_t_id);
    }

    logerror(status);   //not forget!!!if transport is already in use, critical error!!!
    m_hasTransport = true;
}

void PjsipManager::logout()
{
    pj_status_t status = pjsua_acc_set_registration(m_pjAccID,PJ_FALSE);
    logerror(status);
}

void PjsipManager::closeTransport()
{

    pj_status_t status = pjsua_transport_close(m_pjTrID, false);
    logerror(status);
    m_hasTransport = false;
}


void PjsipManager::makeCall(QString dst_uri)
{
    QString uri;
    if(dst_uri.at(0) == "s")
    {
        uri = dst_uri;
    }
    else
    {
        uri = "sip:" + dst_uri + "@" + AppSettings::getAppSettings()->getDomainStr();
    }
    uriAddTrType(uri);
    std::string s1 = uri.toStdString();
    char* sip_dst_uri = const_cast<char*>(s1.c_str());
    pjsua_call_id CallID;
    pj_str_t str = pj_str(sip_dst_uri);
    pj_status_t status = pjsua_call_make_call(m_pjAccID, &str, 0, NULL, NULL, &CallID);
    logerror(status);
}

void PjsipManager::hangUpCall(pjsua_call_id callID)
{
    pj_status_t status = pjsua_call_hangup(callID, 0, NULL, NULL);

    logerror(status);
}

void PjsipManager::hangUpALLcall()
{
    pjsua_call_hangup_all();
}

void PjsipManager::callAnswer(pjsua_call_id callID)
{
    pj_status_t status = pjsua_call_answer(callID, 200, NULL, NULL);
}

void PjsipManager::hold(pjsua_call_id callID)
{
    pj_status_t status = pjsua_call_set_hold(callID, NULL);
    logerror(status);
}

void PjsipManager::unhold(pjsua_call_id callID)
{
    pj_status_t status = pjsua_call_reinvite(callID, PJSUA_CALL_UNHOLD, NULL);
    logerror(status);
}

void PjsipManager::onRegState2(pjsua_acc_id accID, pjsua_reg_info *info)
{
    pjsua_acc_info inf;
    pjsua_acc_get_info(accID, &inf);

    if (info->renew)
    {
        if (inf.status == PJSIP_SC_OK)
        {
            PjsipManager::getInstance()->hasRegistration(true);
        }
        else
        {
            PjsipManager::getInstance()->hasRegistration(false);
        }
    }
    else
    {
        PjsipManager::getInstance()->hasRegistration(false);
    }

}

void PjsipManager::onCallState(pjsua_call_id callID, pjsip_event *e)
{
    pjsua_call_info ci;

    PJ_UNUSED_ARG(e);

    pjsua_call_get_info(callID, &ci);

    //need to look PJLOG
    //PJ_LOG(3, (THIS_FILE, "Call %d state=%.*s", call_id, (int)ci.state_text.slen,ci.state_text.ptr));

    PjsipManager *manager = getInstance();

    if (ci.state == PJSIP_INV_STATE_CALLING)
    {
        manager->m_session.push_back(new Session(callID));
        manager->m_session.back()->setCallState(ci.state);
        manager->updateState(manager->m_session.back());
    }

    SessionList::iterator it = manager->m_session.begin();
    for (it; it!=manager->m_session.end(); it++)
    {
        if ((*it)->getCallID() == callID)
        {
            (*it)->setCallState(ci.state);
            if (ci.state == PJSIP_INV_STATE_DISCONNECTED)
            {                
                manager->updateState((*it));
                it = manager->m_session.erase(it);
                break;
            }
            else if (ci.state == PJSIP_INV_STATE_EARLY || ci.state == PJSIP_INV_STATE_CONNECTING || ci.state == PJSIP_INV_STATE_CONFIRMED)
            {
                manager->updateState((*it));
                break;
            }
        }
    }
}

void PjsipManager::onCallMediaState(pjsua_call_id callID)
{
    pjsua_call_info ci;

    pjsua_call_get_info(callID, &ci);

    PjsipManager *manager = getInstance();
    SessionList::iterator it = manager->m_session.begin();

    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE)
    {
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);
    }

    for (it; it!=manager->m_session.end(); it++)
    {
        if ((*it)->getCallID() == callID)
        {
            (*it)->setCallMediaState(ci.media_status);
            manager->updateMediaState((*it));
            break;
        }
    }
}

void PjsipManager::onIncomingCall(pjsua_acc_id accID, pjsua_call_id callID, pjsip_rx_data *rdata)
{

    pjsua_call_info ci;

    PJ_UNUSED_ARG(accID);
    PJ_UNUSED_ARG(rdata);

    pjsua_call_get_info(callID, &ci);

    PjsipManager *manager = getInstance();
    if (ci.state == PJSIP_INV_STATE_INCOMING)
    {
        manager->m_session.push_back(new Session(callID));
        manager->m_session.back()->setCallState(ci.state);
        manager->updateState(manager->m_session.back());
    }

    //PJ_LOG(3, (THIS_FILE, "Incoming call from %.*s!!", (int)ci.remote_info.slen,ci.remote_info.ptr));

    // Automatically answer incoming calls with 200/OK
    //pjsua_call_answer(call_id, 200, NULL, NULL);
}

void PjsipManager::onTranspState(pjsip_transport *tp, pjsip_transport_state state, const pjsip_transport_state_info *info)
{
    if (state == PJSIP_TP_STATE_DESTROY)
    {
        qInfo() << "PJSIP_TP_STATE_DESTROY";
    }
}

void PjsipManager::logerror(pj_status_t st)
{
    if (st != PJ_SUCCESS)
    {
        char *buf = new char[1024];
        pj_str_t str = pj_strerror(st, buf, 1024);
        qInfo() << buf;
        emit error(buf);
    }
}

void PjsipManager::uriAddTrType(QByteArray &arr)
{
    if (AppSettings::getAppSettings()->getTransportType() == TR_TYPE_TCP)
    {
        arr.append(";transport=tcp");
    }
    else if (AppSettings::getAppSettings()->getTransportType() == TR_TYPE_UDP)
    {
        arr.append(";transport=udp");
    }
}

void PjsipManager::uriAddTrType(QString &str)
{
    if (AppSettings::getAppSettings()->getTransportType() == TR_TYPE_TCP)
    {
        str.append(";transport=tcp");
    }
    else if (AppSettings::getAppSettings()->getTransportType() == TR_TYPE_UDP)
    {
        str.append(";transport=udp");
    }
}


