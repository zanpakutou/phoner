#ifndef APPSETTINGS
#define APPSETTINGS

#include <QSettings>
#include <QString>
#include <utils.h>

#define SETTING_SERVER "/Settings/Registration/server"
#define SETTING_DOMAIN "/Settings/Registration/domain"
#define SETTING_PORT "/Settings/Registration/port"
#define SETTING_TR_TYPE "/Settings/Registration/transport type"
#define SETTING_LOGIN "/Settings/Registration/login"
#define SETTING_PASSWD "/Settings/Registration/password"

class AppSettings: public QObject
{
    Q_OBJECT

private:
    QSettings* m_psettings;

protected:
    AppSettings();
    static AppSettings* m_instance;

public:
    static AppSettings* getAppSettings();
    void setServer(QString);
    void setPort(QString);
    void setTransportType(QString);
    void setLogin(QString);
    void setPasswd(QString);
    void setDomain(QString);
    void save();
    QString getServerStr();
    int getPort();
    QString getTransportType();
    QString getLoginStr();
    QString getPasswdStr();
    QString getDomainStr();
    char* getServerCh();
    char* getLoginCh();
    char* getPasswdCh();
    char* getDomainCh();
};

#endif // APPSETTINGS

