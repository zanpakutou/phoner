#include "ringspanel.h"
#include "ui_ringspanel.h"
#include "session.h"
#include <QDebug>

RingsPanel::RingsPanel(QWidget *parent,Session* pSess):
    QWidget(parent),
    ui(new Ui::RingsPanel)
{
    ui->setupUi(this);
    ui->pushButtonHold->hide();
    m_pSess = pSess;
    ui->pushButtonHangUp->setIcon(QIcon(":/res/icons/hangup.png"));
    ui->pushButtonHold->setIcon(QIcon(":/res/icons/pause.png"));
    ui->labelTimer->setText("00:00:00");
    m_timer = new QTimer;
    ui->labelRemoteName->setText(m_pSess->getRemoteURI());
    connect(ui->pushButtonHangUp, SIGNAL(clicked()), this, SLOT(hangUp()));
    connect(ui->pushButtonHold, SIGNAL(clicked()), this, SLOT(hold()));
}

RingsPanel::~RingsPanel()
{
    delete ui;
    delete m_timer;
}

void RingsPanel::updateTimer()
{
    ui->labelTimer->setText(m_pSess->getTimer().toString());
}

void RingsPanel::startUItimer()
{
    m_timer->start(1000);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updateTimer()));
}

void RingsPanel::hangUp()
{
    PjsipManager::getInstance()->hangUpCall(m_pSess->getCallID());
}

void RingsPanel::hold()
{
    if (ui->pushButtonHold->isChecked())
    {
        PjsipManager::getInstance()->hold(m_pSess->getCallID());
    }
    else
    {
        PjsipManager::getInstance()->unhold(m_pSess->getCallID());
    }
}

void RingsPanel::updateStateRPanel(pjsip_inv_state state)
{
    if (state == PJSIP_INV_STATE_CALLING || state == PJSIP_INV_STATE_CONNECTING)
    {
        startUItimer();
    }
    else if (state == PJSIP_INV_STATE_CONFIRMED)
    {
        ui->pushButtonHold->show();
    }
}

void RingsPanel::updateMediaStateRPanel(pjsua_call_media_status state)
{
    if (state == PJSUA_CALL_MEDIA_REMOTE_HOLD)
    {
        ui->pushButtonHold->setChecked(true);
    }
    else if (state == PJSUA_CALL_MEDIA_ACTIVE)
    {
        ui->pushButtonHold->setChecked(false);
    }
}
