#include "settingwidget.h"
#include "ui_settingwidget.h"
#include "pjsipmanager.h"
#include "pjsipaudiomanager.h"
#include <QtMultimedia/QAudioDeviceInfo>

SettingWidget::SettingWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingWidget)
{
    ui->setupUi(this);
    m_audioManager = new PjsipAudioManager();
    loadSetting();
    connect(ui->pushButtonSaveSetting, SIGNAL(clicked()), this, SLOT(saveSettings()));
    connect(PjsipManager::getInstance(), SIGNAL(hasRegistration(bool)), this, SLOT(hasRegistr(bool)));
}

SettingWidget::~SettingWidget()
{
    AppSettings::getAppSettings()->save();
    delete ui;
}

void SettingWidget::saveSettings()
{
    AppSettings* settings = AppSettings::getAppSettings();

    if((ui->lineEditPort->text() != QString::number(settings->getPort())) || (ui->comboBoxTrType->currentText() != settings->getTransportType()))
    {
        PjsipManager::getInstance()->closeTransport();
    }
    settings->setDomain(ui->lineEditDomain->text());
    settings->setServer(ui->lineEditServer->text());
    settings->setPort(ui->lineEditPort->text());
    settings->setTransportType(ui->comboBoxTrType->currentText());
    settings->save();
    int micr,dyn;
    QList<AbstractAudioManager::AudioDevice>::iterator it = m_deviceList.begin();
    for(it; it!=m_deviceList.end(); it++)
    {
        if((*it).name == ui->comboBoxInput->currentText())
        {
            micr = (*it).id;
        }
        else if((*it).name == ui->comboBoxOutput->currentText())
        {
            dyn = (*it).id;
        }
    }
    m_audioManager->changeAudioDevice(micr,dyn);
}

void SettingWidget::loadSetting()
{
    AppSettings* settings = AppSettings::getAppSettings();

    ui->lineEditPort->setText(QString::number(settings->getPort()));
    ui->lineEditServer->setText(settings->getServerStr());
    ui->comboBoxTrType->setCurrentText(settings->getTransportType());
    ui->lineEditDomain->setText(settings->getDomainStr());

    m_deviceList = m_audioManager->getAudioDeviceList();
    QList<AbstractAudioManager::AudioDevice>::iterator it = m_deviceList.begin();
    QString micr = QAudioDeviceInfo::defaultInputDevice().deviceName();
    int setmicr = -1;
    int setdyn = -1;
    QString dyn = QAudioDeviceInfo::defaultOutputDevice().deviceName();
    for(it; it!=m_deviceList.end(); it++)
    {
        if((*it).type == AbstractAudioManager::DeviceType(0))
        {
            ui->comboBoxOutput->addItem((*it).name);
            int f = dyn.indexOf((*it).name);
            if (f != -1)
            {
                ui->comboBoxOutput->setCurrentText((*it).name);
                setdyn = (*it).id;
            }

        }
        else if((*it).type == AbstractAudioManager::DeviceType(1))
        {
            ui->comboBoxInput->addItem((*it).name);
            int f = micr.indexOf((*it).name);
            if (f != -1)
            {
                ui->comboBoxInput->setCurrentText((*it).name);
                setmicr = (*it).id;
            }

        }
        else if((*it).type == AbstractAudioManager::DeviceType(2))
        {
            ui->comboBoxOutput->addItem((*it).name);
            ui->comboBoxInput->addItem((*it).name);
            int f = dyn.indexOf((*it).name);
            if (f != -1)
            {
                ui->comboBoxOutput->setCurrentText((*it).name);
                setdyn = (*it).id;
            }
            f = micr.indexOf((*it).name);
            if (f != -1)
            {
                ui->comboBoxInput->setCurrentText((*it).name);
                setmicr = (*it).id;
            }
        }        
    }
    if(setdyn != -1 && setmicr != -1)
    {
        m_audioManager->changeAudioDevice(setmicr, setdyn);
    }

}

void SettingWidget::hasRegistr(bool has)
{
    if (has)
    {
       ui->tabServer->setDisabled(true);

    }
    else
    {
        ui->tabServer->setDisabled(false);
    }
}
